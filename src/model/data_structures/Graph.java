package model.data_structures;

import java.util.Iterator;

/**
 * Clase que modela un grafo no dirigido.
 *
 * @param <K> Clase generica que modela una llave de un vertice del grafo.
 * @param <V> Clase generica que modela el valor guardado en los vertices del grafo.
 * @param <A> Clase generica que modela el valor guardado en un arco.
 */
public class Graph<K extends Comparable<K>,V,A> {


	//Atributos

	/**
	 * Numero de vertices que hay en el grafo en el momento
	 */
	private int numVertices;

	/**
	 * Numero de arcos que hay en el Grafo en el momento
	 */
	private int numArcos;
	
	/**
	 * Caso especial indica que solo se va a agregar a v�rtices los valores de keys que ya existen
	 */
	private boolean casoEspecial= false;

	/**
	 * Vertices del grafo 
	 */
	@SuppressWarnings("rawtypes")
	private SeparateChainingHash<K,Vertice> vertices;
	private SeparateChainingHash<Double,Arco> arcos;

	//Constructor


	/**
	 * Construye un grafo del tama�o especificado
	 * @param tamano
	 */
	@SuppressWarnings("rawtypes")
	public Graph(int tamano) {
		if (tamano < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
		numVertices = 0;
		numArcos = 0;
		vertices = new SeparateChainingHash<K,Vertice>(tamano);
		arcos = new SeparateChainingHash<Double,Arco>(1650000);
	}



	//Metodos

	/**
	 * Retorna el numero de Vertices
	 * @return int numero de vertices.
	 */

	public int V() {
		return vertices.getAux();
	}


	/**
	 * Retorna el numero de Arcos  
	 * @return int numero de arcos.
	 */
	public int E()
	{
		return arcos.getAux();
	}

	
	/**
	 * Cambia el valor de casoEspecial a uno dado por par�metro
	 * @param gg
	 */
	public void setCasoEspecial(boolean gg)
	{
		casoEspecial = gg;
	}
	/**
	 * A�ade un vertice a el grafo.
	 * @param idVertex Llave del grafo que se va a a�adir
	 * @param infoVertex Informacion que va a contener el grafo.
	 */

	public void addVertex(K idVertex, V infoVertex)
	{
		if(casoEspecial == true){
			Vertice<K, V> vertice = new Vertice<K, V>(idVertex, infoVertex); 
			vertices.putEspecial(idVertex, vertice);
		}
		else{
		Vertice<K, V> vertice = new Vertice<K, V>(idVertex, infoVertex); 
		vertices.put(idVertex, vertice);
		}
	}

	/**
	 * Valida si el vertice existe
	 * @param llave La llave del vertice.
	 */
	private void validateVertex(K llave) {
		if(vertices.get(llave).equals(null))
			throw new IllegalArgumentException("El vertice no existe");
	}

	/**
	 * A�ade un arco al grafo
	 * @param idVertexIni Vertice ininial el arco
	 * @param idVertexFin Vertice final del arco
	 * @param infoArc Informacion del arco
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) {
		validateVertex(idVertexIni);
		validateVertex(idVertexFin);
	
		
		Vertice ver = vertices.get(idVertexIni);
		Iterator<Arco> adj = ver.adyacentes();
		
		while(adj.hasNext())
		{
			K aa = (K)((Arco)adj.next()).darOtroVertice(ver);
			if(aa.equals(idVertexFin))
				return;
		}
		
		Arco arco = new Arco(idVertexIni, idVertexFin, infoArc);
		vertices.get(idVertexIni).add(arco);
		vertices.get(idVertexFin).add(arco);
		numArcos++;
		arcos.put((Double)infoArc, arco);
	}


	/**
	 * Da la informacion del vertice con la llave dada.
	 * @param idVertex Llave del vertice que se quiere revisar 
	 * @return V Informacion del vertice
	 */
	@SuppressWarnings("unchecked")
	public V getInfoVertex(K idVertex)
	{
		return (V) vertices.get(idVertex).darValor();
	}

	/**
	 * Cambia la informacion del vertice con la llave dada por el valor dado.
	 * @param idVertex Llave del vertice al cual sse le va a cambiar la informacion.
	 * @param infoVertex Informacion nueva.
	 */
	@SuppressWarnings("unchecked")
	public void setInfoVertex(K idVertex, V infoVertex)
	{
		vertices.get(idVertex).cambiarValor(infoVertex);
	}

	/**
	 * Retorna la informacion del arco que conecta los dos vertices.
	 * @param idVertexIni 
	 * @param idVertexFin
	 * @return A informacion del arco buscado
	 */
	@SuppressWarnings({ "unchecked" })
	public A getInfoArc(K idVertexIni, K idVertexFin)
	{
		return (A) vertices.get(idVertexIni).getArco(idVertexFin).darInfo();
	}
	


	/**
	 * Cambia la informacion del arco por la pasada por parametro.
	 * @param idVertexIni Vertice inicial del arco
	 * @param idVertexFin Vertice final del arco
	 * @param infoArc Informacion nueva del arco.
	 */
	@SuppressWarnings("unchecked")
	public void setInfoArc(K idVertexIni, K idVertexFin,A infoArc)
	{
		vertices.get(idVertexIni).getArco(idVertexFin).cambiarInfo(infoArc);
	}
	

	/**
	 * Iterador de las llaves de los vertices adyacentes al vertice pasado por parametro.
	 * @param idVertex Id del vertice del cual se neesitan los adyacentes.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Iterator<Arco> adj(K idVertex) 
	{
		if(vertices.get(idVertex)!=null)
		return vertices.get(idVertex).adyacentes();
		else
		return null;
	}

	public Iterator<K> verts() 
	{
		return vertices.keys(); 	
	}
	
	public Iterator<Double> arcs() 
	{
		return arcos.keys(); 	
	}
	
	public Arco getArc(Double el)
	{
		return arcos.get(el);
	}

	@SuppressWarnings("rawtypes")
	public SeparateChainingHash<K, Vertice> vertices()
	{
		return vertices;
	}
	
	public SeparateChainingHash<Double, Arco> arcos()
	{
		return arcos;
	}
}
