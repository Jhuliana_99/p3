package model.data_structures;

import java.util.Iterator;

import reader.Way;

public class CC {
	
	 private SeparateChainingHash<String, Boolean> marked;   // marked[v] = has vertex v been marked?
	    private SeparateChainingHash<String, Integer> id;           // id[v] = id of connected component containing v
	    private SeparateChainingHash<Integer, Integer> size;         // size[id] = number of vertices in given component
	    private int count;          // number of connected components
	    private SeparateChainingHash<Integer, Graph> subgrafos;
	    
	    
	    
	    public CC(Graph G) {
	    	count = 0;
	        marked = new SeparateChainingHash<String, Boolean>(G.V());
	        id = new SeparateChainingHash<String, Integer>(G.V());
	        size = new SeparateChainingHash<Integer, Integer>(G.V());
	        
	        Iterator<String> iterador = G.verts();
	        
	        while (iterador.hasNext()) {
	        	String v = iterador.next();
	            if (!(marked.get(v+""))) {
	                dfs(G, Integer.parseInt(v));
	                count++;
	            }
	        }
	    }
	    private void dfs(Graph G, int v) {
	        marked.put(""+v, true);
	        id.put(""+v, count);
	        
	        int temp = size.get(count);
	        size.replace(count, temp++);
	        
	        
	        Iterator<String> iterador = G.adj(v);
	        
	        while(iterador.hasNext())
	        {
	        	 int w = Integer.parseInt(iterador.next());
	        	 if (!marked.get(""+w)) {
		                dfs(G, w);
		            }
	        }
	        
	    }
	    
	    public int id(int v) {
	        return id.get(""+v);
	    }
	    
	    public int size(int v) {
	        return size.get(id.get(""+v));
	    }
	    
	    public int count() {
	        return count;
	    }
	    
	    public boolean connected(int v, int w) {
	        return id(v) == id(w);
	    }
	    
	    

}
