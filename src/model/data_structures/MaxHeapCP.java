package model.data_structures;

import java.util.Comparator;

public class MaxHeapCP<T>{

	/**
	 * Tama�o de la lista
	 */
	private int size;

	/**
	 * Arreglo que contiene los elementos de la lista.
	 */
	private Comparable<T>[] arreglo;

	/**
	 * Criterio de comparaci�n del heap de prioridad
	 */
	private Comparator criterioDeComparacion; 


	//Constructor
	public MaxHeapCP(int tama�o, Comparator criterio)
	{
		size = 0;
		arreglo = new Comparable[tama�o+1];
		criterioDeComparacion = criterio;
	}

	public int darNumElementos() {
		return size;
	}

	public void agregar(T elemento) {
		if(size == arreglo.length-1)
		{
			arreglo[size] = null; 
			arreglo[size] = (Comparable<T>)elemento;
			swim(size);
		}
		else{
			arreglo[++size] = (Comparable<T>)elemento;
			swim(size);
		}
	}


	public T delMax() {

		if (esVacia()) return null;
		T max = (T) arreglo[1];
		exchange(1, size--);
		sink(1);
		arreglo[size+1] = null;     
		return max;
	}


	public void swim(int k) {
		while (k > 1 && less(k, k/2)) {
			exchange(k, k/2);
			k = k/2;
		}	
	}


	public void sink(int k) {
		while (2*k <= size) {
			int j = 2*k;
			if (j < size && less(j+1,j)) j++;
			if (!less(j,k)) break;
			exchange(k, j);
			k = j;
		}
	}


	public boolean esVacia() {

		return size == 0;
	}

	/**
	 * Intercambia dos elementos dadas sus posiciones.
	 * @param i, posici�n del primer elemento a intercambiar.
	 * @param j, posici�n del segundo elemento a intercambiar.
	 */
	public void exchange(int i, int j)
	{
		Comparable t = arreglo[i]; arreglo[i] = arreglo[j]; arreglo[j] = t; 
	}

	/**
	 * Indica si el elemento en el primer �ndice dado es menor que el elemento del segundo �ndice.
	 * @param i �ndice del primer elemento a comparar.
	 * @param j �ndice del segundo elemento a comparar.
	 * @return true si es menor, false en caso contrario.
	 */
	public boolean less(int i, int j)
	{
		if (criterioDeComparacion != null) {
			return criterioDeComparacion.compare(arreglo[i], arreglo[j]) < 0;
		}
		else if(arreglo[i] instanceof Double)
		{
			return (Double)arreglo[i] <= (Double)arreglo[j];
		}
		else{
			return ((Comparable<T>) arreglo[i]).compareTo((T) arreglo[j]) < 0;
		}


	}
	
	public Comparable<T>[] darArreglo() {
		return arreglo;
	}



	public T max() {
		return (T)arreglo[1];
	}
}
