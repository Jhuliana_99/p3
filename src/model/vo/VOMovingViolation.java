package model.vo;

import com.opencsv.bean.CsvBindByName;
/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {


	@CsvBindByName(column = "OBJECTID_1")
	private int OBJECTID_1;

	@CsvBindByName(column = "OBJECTID")
	private int OBJECTID;

	@CsvBindByName(column = "ROW_", required=false)
	private String row_;

	@CsvBindByName(column = "LOCATION")
	private String location;

	@CsvBindByName(column = "ADDRESS_ID", required=false)
	private String address_id;

	@CsvBindByName(column = "STREETSEGI")
	private String streetsegi;

	@CsvBindByName(column = "XCOORD")
	private String xcoord;

	@CsvBindByName(column = "YCOORD")
	private String ycoord;

	@CsvBindByName(column = "TICKETTYPE")
	private String tickettype;

	@CsvBindByName(column = "FINEAMT")
	private int fineamt;

	@CsvBindByName(column = "TOTALPAID")
	private int totalpaid;

	@CsvBindByName(column = "PENALTY1")
	private String penalty1;

	@CsvBindByName(column = "PENALTY2", required=false)
	private String penalty2;

	@CsvBindByName(column = "ACCIDENTIN")
	private String ACCIDENTIN;

	@CsvBindByName(column = "AGENCYID", required=false)
	private String agencyid;

	@CsvBindByName(column = "TICKETISSU")
	private String TICKETISSU;

	@CsvBindByName(column = "VIOLATIONC")
	private String violationc;

	@CsvBindByName(column = "VIOLATIOND")
	private String violationd;

	@CsvBindByName(column = "ROW_ID")
	private String row_id;

	@CsvBindByName(column = "LAT")
	private String lat;

	@CsvBindByName(column = "LONG")
	private String Long;

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int getOBJECTID_1() {
		return OBJECTID_1;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int getOBJECTID() {
		return OBJECTID;
	}

	/**
	 * No sirve para nada pero es necesario para que openscv pueda construir el objeto :))
	 */
	public String getRow_() {
		return row_;
	}

	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return address_id - retorna la ID de la direcci�n.
	 */
	public String getAddress_id()
	{
		return address_id;
	}

	/**
	 * @return streetsegeid - retorna la ID del segmento de la calle.
	 */
	public String getStreetsegi()
	{
		return streetsegi;
	}

	/**
	 * @return xcoord - retorna la coordenada x donde sucedi� la infracci�n.
	 */
	public String getXcoord()
	{
		return xcoord;
	}

	public String getAgencyid()
	{
		return agencyid;
	}

	/**
	 * @return ycoord - retorna la coordenada y donde sucedi� la infracci�n.
	 */
	public String getYcoord()
	{
		return ycoord;
	}

	/**
	 * @return tickettype.
	 */
	public String getTickettype()
	{
		return tickettype;
	}

	/**
	 * @return fineamt - retorna la cantidad a pagar por la infracci�n en USD.
	 */
	public int getFineamt()
	{
		return fineamt;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public int getTotalPaid() {
		return totalpaid;
	}

	/**
	 * @return penalty1 - retorna el dinero extra que debe pagar el conductor.
	 */
	public String getPenalty1()
	{
		return penalty1;
	}

	/**
	 * @return penalty2 - retorna el dinero extra que debe pagar el conductor.
	 */
	public String getPenalty2()
	{
		return penalty2;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getACCIDENTIN() {
		return ACCIDENTIN;
	}

	/**
	 * @return date - retorna la fecha cuando se puso la infracci�n .
	 */
	public String getTICKETISSU() {
		return TICKETISSU;
	}

	/**
	 * @return violationcode - retorna el c�digo de la infracci�n.
	 */
	public String  getViolationc() {
		return violationc;
	}

	/**
	 * @return row_id
	 */
	public String getrow_id()
	{
		return row_id;
	}

	/**
	 * @return violatuondesc - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationD() {
		return violationd;
	}

	public String getLat() {
		return lat;
	}

	public String getLong() {
		return Long;
	}


	/**
	 * Le da un valor al identificador �nico de la infracci�n.
	 * @param objectId, Identificador �nico de la infracci�n.
	 */
	public void setOBJECTID_1(int objectId) {
		OBJECTID_1 = objectId;
	}

	/**
	 * Le da un valor al identificador �nico de la infracci�n.
	 * @param objectId, Identificador �nico de la infracci�n.
	 */
	public void setOBJECTID(int objectId) {
		OBJECTID = objectId;
	}

	/**
	 * Le da un valor a row_
	 * @param r
	 */
	public void setRow_(String r) {
		row_ = r;
	}

	/**
	 * Le da un valor a la ID de la direcci�n.
	 * @param ad, ID de la direcci�n.
	 */
	public void setAddress_id(String ad)
	{
		address_id = ad;
	}


	/**
	 * Le da un valor a la direcci�n.
	 * @param loc, direcci�n.
	 */
	public void setLocation(String loc) {
		location = loc;
	}

	/**
	 * Le da un valor a la ID del segmento de la calle.
	 * @param ssd, ID del segmento de la calle.
	 */
	public void setStreetsegi( String ssd)
	{
		streetsegi = ssd;
	}

	/**
	 * Le da un valor a la coordenada x .
	 * @param x, coordenada x.
	 */
	public void setXcoord(String x)
	{
		xcoord = x;
	}

	/**
	 * Le da un valor a la coordenada y.
	 * @param y, coordenada y.
	 */
	public void setYcoord(String y)
	{
		ycoord = y;
	}

	/**
	 */
	public void setTickettype(String tt)
	{
		tickettype = tt;
	}

	public void setAgencyid(String tt)
	{
		agencyid = tt;
	}

	/**
	 * Le da un valor a la cantidad a pagar por la infracci�n en USD.
	 * @param fa, la cantidad a pagar por la infracci�n en USD.
	 */
	public void setFineamt( int fa)
	{
		fineamt = fa;
	}


	/**
	 * Le da un valor al dinero pagado por la persona que realiz� la infracci�n.
	 * @param tp, dinero pagado por la persona que realiz� la infracci�n.
	 */
	public void setTotalPaid(int tp) {
		totalpaid= tp;
	}

	/**
	 * Le da un valor al dinero extra que tiene pagar el conductor.
	 * @param p1, dinero extra que tiene pagar el conductor.
	 */
	public void getPenalty1(String p1)
	{
		penalty1= p1;
	}

	/**
	 * Le da un valor al dinero extra que tiene pagar el conductor.
	 * @param p2, dinero extra que tiene pagar el conductor.
	 */
	public void setPenalty2(String p2)
	{
		penalty2= p2;
	}

	/**
	 * Le da un valor a la fecha cuando se puso la infracci�n .
	 * @param tid - Fecha cuando se puso la infracci�n .
	 */
	public void setTICKETISSU(String tid) {
		TICKETISSU = tid;
	}


	/**
	 * Le da un valor al indicador de accidentes
	 * @rparam ai - Si hubo un accidente o no.
	 */
	public void  setACCIDENTIN(String ai) {
		ACCIDENTIN = ai;
	}

	/**
	 * @return violationcode - retorna el c�digo de la infracci�n.
	 */
	public void setViolationc(String v) {
		violationc = v;
	}

	/**
	 * Le da un valor a la Descripci�n textual de la infracci�n.
	 * vd, descripci�n textual de la infracci�n..
	 */
	public void  setViolationD(String vd) {
		violationd = vd;
	}

	/**
	 * 
	 * @param ri
	 */
	public void setRow_id(String ri)
	{
		row_id = ri;
	}

	public void setLat(String v) {

		lat = v;
	}

	public void setLong(String v) {
		Long = v;
	}


	/**
	 * Retorna una cadena de texto con algunos datos de la infracci�n
	 */
	public String toString() {

		return OBJECTID + row_  + " | "+ TICKETISSU + " | " + location + " | " + violationd + "\n";
	}

	@Override
	public int compareTo(VOMovingViolation arg0) {

		String isdt2 = arg0.getTICKETISSU();

		if(TICKETISSU.equals(isdt2))
		{
			return compareToId(arg0);
		}

		else if(TICKETISSU.compareToIgnoreCase(isdt2) > 0)
			return 1;
		else
			return -1;

	}

	public int compareToId(VOMovingViolation arg0) {

		int isdt2 = arg0.getOBJECTID();

		if(OBJECTID == isdt2)
			return 0;

		else if(OBJECTID > isdt2)
			return 1;
		else
			return -1;

	}
}
