package comparators;

import java.util.Comparator;

import model.data_structures.Graph;
import model.data_structures.Vertice;
import reader.Node;
import reader.Way;

public class ByNum implements Comparator<String>{

	private Graph<String, Node, Way> grafo;
	
	private boolean orden;
	
	public ByNum(Graph pGrafo, boolean pOrden)
	{
		grafo = pGrafo;
		
		orden = pOrden;
	}
	
	@Override
	public int compare(String o1, String o2) {

		int inf1 = grafo.getInfoVertex(o1).getInfraccionesCercanas().getSize(); 
		int inf2 = grafo.getInfoVertex(o2).getInfraccionesCercanas().getSize();
		
		if(orden)
		{
			if(inf1>inf2)
				return 1;
			else if(inf1<inf2)
				return -1;
			else
				return 0;
		}else
		{
			if(inf1<inf2)
				return 1;
			else if(inf1>inf2)
				return -1;
			else
				return 0;
		}
	}

}
