package comparators;

import java.util.Comparator;

import model.data_structures.Arco;
import model.data_structures.Graph;
import model.data_structures.Vertice;
import reader.Node;
import reader.Way;

public class ByDistance implements Comparator<Arco>{


	private Graph grafo;
	
	private boolean orden;
	
	public ByDistance(Graph pGrafo, boolean pOrden)
	{
		grafo = pGrafo;
		
		orden = pOrden;
	}
	
	@Override
	public int compare(Arco o1, Arco o2) {

		double lat1 = ((Node)grafo.getInfoVertex((String) o1.darVerticeIni())).getLat();
		double lon1 = ((Node)grafo.getInfoVertex((String) o1.darVerticeIni())).getlon();
		double lat2 = ((Node)grafo.getInfoVertex((String) o1.darVerticeFin())).getLat();
		double lon2 = ((Node)grafo.getInfoVertex((String) o1.darVerticeFin())).getlon();
		
		double inf1 = calcularHaversine(lat1, lat2, lon1, lon2); 
		
		lat1 = ((Node)grafo.getInfoVertex((String) o2.darVerticeIni())).getLat();
		lon1 = ((Node)grafo.getInfoVertex((String) o2.darVerticeIni())).getlon();
		lat2 = ((Node)grafo.getInfoVertex((String) o2.darVerticeFin())).getLat();
		lon2 = ((Node)grafo.getInfoVertex((String) o2.darVerticeFin())).getlon();
		
		double inf2 = calcularHaversine(lat1, lat2, lon1, lon2);  
		
		if(orden)
		{
			if(inf1>inf2)
				return 1;
			else if(inf1<inf2)
				return -1;
			else
				return 0;
		}else
		{
			if(inf1<inf2)
				return 1;
			else if(inf1>inf2)
				return -1;
			else
				return 0;
		}
	}


		public  double calcularHaversine(double lat1, double lat2, double lon1, double lon2)
		{
			double restaLat = lat2-lat1;
			double restaLon = lon2-lon1;
			double difLatitud =Math.toRadians(restaLat);
			double difLongitud= Math.toRadians(restaLon);

			double a = Math.pow(Math.sin(difLatitud/2),2) +
					Math.cos(Math.toRadians(lat1))*
					Math.cos(Math.toRadians(lat2))*
					Math.pow(Math.sin(difLongitud/2),2);
			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

			return c*6371.01;
		}

}
