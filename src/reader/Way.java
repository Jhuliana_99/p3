package reader;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.LinkedList;
import model.vo.VOMovingViolation;

public class Way {

	//XML element id
	private int id;
//	//XML element lon
	private LinkedList nd;
	
	private boolean highway;

	public Way()
	{
		nd = new LinkedList();
	}
	public int getId() {
		return id;	
		}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean getHighway() {
		return highway;	
		}
	
	public void setHighway(boolean id) {
		this.highway = id;
	}

	public void setNd(LinkedList val) {
		nd = val;
	}
	public LinkedList getNd() {
		return nd;
	}

	public String toString() {
		
		String rta;
		rta = this.id + ": \n" ;
		Iterator<String> k = nd.iterator();
		while(k.hasNext()){
		rta += "   " + nd.next();
	}
		return rta;
	}
}
