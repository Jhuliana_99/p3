package reader;

import model.data_structures.LinkedList;
import model.vo.VOMovingViolation;

public class Node {

	//XML element id
	private String id;
	//XML element lon
	private double lon;
	//XML element lat
	private double lat;
	//XML element tag
	private double tag;
	/**
	 * Representa la lista con las infracciones m�s cercanas
	 */
	private LinkedList<Integer> lista = new LinkedList<Integer>();
	
	public Node(String idd, double latt, double lonn)
	{
		this.id = idd;
		this.lat = latt;
		this.lon = lonn;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getlon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}

	public LinkedList<Integer> getInfraccionesCercanas()
	{
		return lista;
	}

	/**
	 * A�ade una infraccion a la lista de infracciones cercanas
	 */
	public void add(Integer g)
	{
		lista.add(g);
	}

	public String toString() {
		return this.id + ":" + this.lat +  ":" + this.lon;
	}
}
